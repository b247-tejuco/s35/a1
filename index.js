//gitbash cmds
//npm init
//npm install mongoose@6.10.0
//npm install express



//initializing
const express= require("express");
const mongoose= require("mongoose");
const app= express();
const port= 3001;
app.use(express.json())
app.use(express.urlencoded({extended: true}));

//mongodb connection
mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/s35-activity?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//show mongodb connection status in console: error/success
let db= mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", ()=> console.log("We're connected to the cloud database"))

// schema (structure/frame/blueprint)
const userSchema= new mongoose.Schema({
	username: String,
	password: String
})

// model **Capital and Singular**
const User= mongoose.model("User", userSchema);

//routes
app.post("/signup", (request, response)=> {

	
	User.findOne({username: request.body.username}, (error, result)=> {
		if(result != null && result.username == request.body.username){
			return response.send('Username already exist!')
		}

		let newUser= new User({
			username: request.body.username,
			password: request.body.password

		})

		newUser.save((error, savedUser)=> {
			if(error){
				return console.error(error)
			} else{
				return response.status(200).send('New user registered')
			}
		})
	})
})

//listen port
app.listen(port, () => console.log(`Server is now running at port ${port}`))